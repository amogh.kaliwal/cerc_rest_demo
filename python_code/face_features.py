## command prompt
#pip install cmake
#pip install dlib -vvv
#pip install face_recognition
#pip install numpy
#pip install cv2

# importing libraries for 
import cv2
import face_recognition
import numpy as np
import os

""" Identify the face and locate it on video """

def identify_face(ImageFilePath, VideoFilePath):
    
    # Videeo file is linked here
    video_capture = cv2.VideoCapture(VideoFilePath)
    
    # Load a sample picture and learn how to recognize it.
    user_image = face_recognition.load_image_file(ImageFilePath)
    user_face_encoding = face_recognition.face_encodings(user_image)[0]
    
    # Create arrays of known face encodings and their names
    known_face_encodings = [
        user_face_encoding
    ]
    known_face_names = [
        "Harish Salve"
    ]
    
    while True:
        # Grab a single frame of video
        ret, frame = video_capture.read()
        
        if not ret:
            break
        
        # Convert the image from BGR color (which OpenCV uses) to RGB 
        # color (which face_recognition uses)
        rgb_frame = frame[:, :, ::-1]
    
        # Find all the faces and face enqcodings in the frame of video
        face_locations = face_recognition.face_locations(rgb_frame)
        face_encodings = face_recognition.face_encodings(rgb_frame, 
                                                         face_locations)
    
        # Loop through each face in this frame of video
        for (top, right, bottom, left), face_encoding in zip(face_locations, 
            face_encodings):
            
            # See if the face is a match for the known face(s)
            matches = face_recognition.compare_faces(known_face_encodings, 
                                                     face_encoding)
    
            name = "Unknown"
            
            # Or instead, use the known face with the smallest distance to t
            # he new face
            face_distances = face_recognition.face_distance(known_face_encodings, 
                                                            face_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = known_face_names[best_match_index]
    
            # Draw a box around the face
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
    
            # Draw a label with a name below the face
            cv2.rectangle(frame, (left, bottom - 35), (right, bottom), 
                          (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, 
                        (255, 255, 255), 1)
    
        # Display the resulting image
        cv2.imshow('Video', frame)
    
        # Hit 'q' on the keyboard to quit!
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    
    # Release handle to the webcam
    video_capture.release()
    cv2.destroyAllWindows()

##############################################################
    
# https://www.mygreatlearning.com/blog/face-recognition/
    
##############################################################
