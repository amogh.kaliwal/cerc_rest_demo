## install required package
#pip install SpeechRecognition
#pip install moviepy
#pip install PythonVideoConverter

# importing libraries for 
import speech_recognition as sr
import moviepy.editor as mp
import constants
import os

"""   Function to convert a video file to audio file    """
def video_audio_to_text(FilePath, FileType):
    
    # defining the recognizer
    r = sr.Recognizer()
    
    # process the video for
    if FileType == "video":
        
        # provide the file path name and 
        clip = mp.VideoFileClip(FilePath)
        
        # identify the audio file name from path        
        audio_filename = FilePath.split("/")[::-1][0].split(".")[0]
        
        # save converted audio file name with suitable name
        clip.audio.write_audiofile(
                os.path.join(constants.base_path, 
                             constants.audio_folder,
                             audio_filename + ".wav"))
                
        # read audio file
        audio_file = sr.AudioFile(os.path.join(constants.base_path, 
                                               constants.audio_folder,
                                               audio_filename + ".wav"))
    
    # if audio file then 
    elif FileType == "audio":
        
        # read audio file
        audio_file = sr.AudioFile(FilePath)
        
        # identify the audio file name from path        
        audio_filename = FilePath.split("/")[::-1][0].split(".")[0]
    
    # Capture audio file Input
    with audio_file as source:
        r.adjust_for_ambient_noise(source, duration=0.5)
        audio = r.record(source)
    
    # convert audio to text output
    result = r.recognize_google(audio)
    
    # exporting the result 
    with open(os.path.join(constants.base_path, 
                           constants.text_folder,
                           audio_filename + '_recognized.txt'), 
              mode = 'w') as file: 
              file.write("Converted Speech:") 
              file.write("\n") 
              file.write(result) 
              print("ready!")
