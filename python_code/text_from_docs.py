## install required package
#pip install PyPDF2

# importing module
import os
import constants
import PyPDF2
import re

"""  function to import a pdf file and extract text  """

def pdf_to_text(FilePath):
        
    # open your file to read
    pdf_file = open(FilePath, 'rb')
    
    # Create A PDF Reader Object
    read_pdf = PyPDF2.PdfFileReader(pdf_file)
    
    # identify the number of pages in pdf file
    number_of_pages = read_pdf.getNumPages()
    
    # identify the audio file name from path        
    text_filename = FilePath.split("/")[::-1][0].split(".")[0] + '.txt'
    text_filepath = os.path.join(constants.base_path, 
                                 constants.text_folder,
                                 text_filename)
    
    # read all the pages of pdf and integrate them
    with open(text_filepath, 'w', encoding = "utf-8") as text_file:
        # loop for all the pages of pdf
        for i in range(number_of_pages):
            
            # read page related to pdf
            page = read_pdf.getPage(i)
            
            # extract contents of the page
            page_content = page.extractText()
            
            # process text for beautification
            # remove notation of move to the next sheet of paper
            page_content = page_content.replace('\x0c','')
            page_content = re.sub(r'\n+', '\n', page_content).strip()
            page_content = page_content.split('\n \n')
            
            # loop to identify the un-wanted new line and remove it
            for j in range(len(page_content)-1):
                page_content[j] = page_content[j].replace('\n','')
            
            # loop to identify the un-wanted new line and remove it
            for k in range(len(page_content)-1):
                
                # write the contents of page to text file
                text_file.writelines(page_content[k] + '\n')
        
        # to change file access modes
        text_file.close()
