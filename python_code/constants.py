#####################################################################

# Base Path Declarations
base_path = "/home/demo/cerc_rest_demo"
audio_folder = "input_data/audio_files/"
video_folder = "input_data/video_files/"
text_folder = "output_data/extracted_text/"
image_folder = "input_data/image_files/"
docs_folder = "input_data/document_files/"

#####################################################################
